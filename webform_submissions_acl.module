<?php
// $Id: webform_submissions_acl.module,v 1.1 2009/10/30 17:49:17 davyvandenbremt Exp $

/**
 * @file
 * Drupal hooks and helper functions.
 *
 * @author Davy Van Den Bremt
 * Modified by: Edouard Poitras <epoitras@layer8.org> on June 27th, 2012 in an attempt
 * to get it working with Drupal 7
 * Thanks to pbosmans (drupal username) for the permissions fix @ http://drupal.org/node/1561834
 * Thanks to Matt B (drupal username) for the module package placement idea @ http://drupal.org/node/1111124
 * Thanks to Zyxtrio (drupal username) for the role based ACL code @ http://drupal.org/node/636612
 */

/**
 * Implementation of hook_menu().
 */
function webform_submissions_acl_menu() {
  $items = array();
  $items['node/%webform_menu/webform/acl'] = array(
    'title' => 'Results access',
    'page callback' => 'webform_submissions_acl_page',
    'page arguments' => array(1),
    'access callback' => 'webform_submissions_acl_admin_access',
    'access arguments' => array(1),
    'file' => 'webform_submissions_acl.pages.inc',
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
  );
  $items['node/%webform_menu/webform/acl/delete/%user'] = array(
    'title' => 'ACL',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webform_submissions_acl_delete_confirm_form', 1, 5),
    'access callback' => 'webform_submissions_acl_admin_access',
    'access arguments' => array(1),
    'file' => 'webform_submissions_acl.pages.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_webform_results_access().
 *
 * Make webform results accessible for users via ACL.
 */
function webform_submissions_acl_webform_results_access($node, $account = NULL) {
  global $user;
  $node_users = webform_submissions_acl_users_by_node($node->nid, TRUE);
  return node_access('view', $node) && in_array($user->uid, $node_users);
}

/**
 * Implements hook_webform_submission_access().
 *
 * Make webform submissions accessible (including individual deletion) for users via ACL.
 */
function webform_submissions_acl_webform_submission_access($node, $submission, $op = 'view', $account = NUL) {
  global $user;
  $node_users = webform_submissions_acl_users_by_node($node->nid, TRUE);
  return node_access('view', $node) && in_array($user->uid, $node_users);
}

/**
 * Menu access callback.
 * Ensure the user has permission to configure ACL.
 */
function webform_submissions_acl_admin_access($node) {
  return user_access('administer webform submissions acl') && node_access('update', $node);
}

/**
 * Implements hook_permission().
 */
function webform_submissions_acl_permission() {
  return array(
    'administer webform submissions acl' => array(
      'title' => t('Administer Webform Submissions ACL'),
      'description' => t('Configure Webform Submissions ACL on all webforms.'),
    ),
  );
}

/**
 * Implementation of hook_theme().
 */
function webform_submissions_acl_theme($existing, $type, $theme, $path) {
  return array(
    'webform_submissions_acl_page' => array(
      'variables' => array(
        'form' => '',
        'node' => NULL,
        'users' => array()
      ),
    ),
  );
}

function webform_submissions_acl_users_by_node($nid, $id_only = FALSE) {
  $users = array();
  $result = db_query("SELECT wsa.uid, u.name FROM {webform_submissions_acl} wsa INNER JOIN {users} u ON u.uid = wsa.uid WHERE wsa.nid = :nid", array(':nid' => $nid));
  foreach ($result as $row) {
    if ($id_only) {
      $users[] = $row->uid;
    }
    else {
      $users[] = $row;
    }
  }
  return $users;
}

function webform_submissions_acl_nodes_by_user($uid, $id_only = FALSE) {
  $nodes = array();
  $result = db_query("SELECT wsa.nid, wsa.title FROM {webform_submissions_acl} wsa INNER JOIN {node} n ON n.nid = wsa.nid WHERE wsa.uid = " . $uid);
  foreach ($result as $row) {
    if ($id_only) {
      $nodes[] = $row->nid;
    }
    else {
      $nodes[] = $row;
    }
  }
  return $nodes;
}

/*function webform_submissions_acl_users_by_role($nid, $rid, $id_only = FALSE) {
  $users = array();
  $result = db_query("SELECT u.uid, u.name, u.status FROM {users} u INNER JOIN {users_roles} ur ON u.uid=ur.uid WHERE ur.rid = %d AND u.status = 1", $rid);
  foreach ($result as $row) {
    if ($id_only) {
      $users[] = $row->uid;
    }
    else {
      $users[] = $row;
    }
  }
  return $users;
} */

function webform_submissions_acl_delete($nid, $uid) {
  db_delete('webform_submissions_acl')->condition('nid', $nid)->condition('uid', $uid)->execute();
}
